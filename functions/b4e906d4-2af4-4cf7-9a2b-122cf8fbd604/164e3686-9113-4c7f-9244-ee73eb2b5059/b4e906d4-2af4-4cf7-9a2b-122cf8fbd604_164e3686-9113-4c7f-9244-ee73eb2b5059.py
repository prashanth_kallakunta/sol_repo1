from xpms_file_storage.file_handler import XpmsResourceFactory, XpmsResource, LocalResource
import pandas as pd
import time
from xpms_storage.db_handler import DBProvider
import uuid
from datetime import datetime
import json
import requests
from xpms_storage.utils import get_env


def hma_metadata_chunk(config=None, **objects):
    NAMESPACE = get_env("NAMESPACE", "claims-audit", False)
    DOMAIN_NAME = get_env("DOMAIN_NAME", "enterprise.xpms.ai", False)
    ENV_DATABASE = get_env('DATABASE_PARAPHRASE', None, True)
    BE_URL = get_env('CLAIMS_AUDIT_APIS_URL', None, True)

    file_path = objects["document"][0]["metadata"]["properties"]["file_metadata"]["file_path"]
    local_csv_path = "/tmp/" + objects["document"][0]["metadata"]["properties"]["filename"]
    minio_resource = XpmsResource.get(urn=file_path)
    local_res = LocalResource(key=local_csv_path)
    minio_resource.copy(local_res)
    file_name = objects["document"][0]["metadata"]["properties"]["filename"]
    # dataset = pd.read_csv(local_csv_path)

    start_time = int(time.time())
    uuid_id = str(uuid.uuid4())[:8]
    # batch_name = "batch_{0}_{1}".format(uuid_id, start_time)
    converted_start_time = datetime.utcfromtimestamp(start_time)
    input_source = objects["document"][0]["metadata"]["properties"]["extension"]
    status = "processing"
    # batch_volume = dataset.groupby("CLAIM_NUMBER_Mask").grouper.shape[0]

    # db = DBProvider.get_instance(db_name=ENV_DATABASE)
    try:
        db = DBProvider.get_instance(db_name=ENV_DATABASE)
        aggregate = [
            {
                "$match": {
                    "threshold": {"$exists": True},
                    "status": "processing"
                }

            },
            {
                "$project": {
                    "batch_name": "$batch_name",
                    "threshold": "$threshold",
                    "file_name_chunk": file_name
                }
            }
        ]
        data = db.find(table='batch_metadata_chunk', aggregate=aggregate)
        threshold = data[0]['threshold']
        batch_name = data[0]['batch_name']

    except Exception as e:
        return 'e is ' + str(e)
    config['context']['threshold'] = threshold
    config["context"]["batch_name"] = batch_name
    config["context"]["start_time"] = start_time
    config["context"]["input_file_name"] = objects["document"][0]["metadata"]["properties"]["filename"]

    update_ob = {
        "input_source": input_source,
        "audit_needed": None,
        "audit_not_needed": None,
        "batch_start_date": start_time,
        "converted_start_time": converted_start_time,
        "status": status,
        "file_name": objects["document"][0]["metadata"]["properties"]["filename"]
    }
    filter_ob = {"file_name_chunk": file_name}
    try:
        db = DBProvider.get_instance(db_name=ENV_DATABASE)
        s = db.update(table='batch_metadata_chunk', update_obj=update_ob, filter_obj=filter_ob)
    except Exception as e:
        return 'e is ' + str(e)

    return objects
