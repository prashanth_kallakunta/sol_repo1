import json

from xpms_file_storage.file_handler import XpmsResourceFactory, XpmsResource, LocalResource
import pandas as pd
from datetime import datetime
import os
from xpms_storage.utils import get_env


def split_hma_check_files(config=None, **objects):
    NAMESPACE = get_env("NAMESPACE", "claims-audit", False)
    DOMAIN_NAME = get_env("DOMAIN_NAME", "enterprise.xpms.ai", False)
    AMAZON_AWS_BUCKET = get_env("AMAZON_AWS_BUCKET", "xpms-ca-test", False)

    # read configurations
    validate_on = config['validate_on']
    inbound_or_outbound = "inbound" if config['validate'] == "inbound" else "outbound"
    number_of_files = json.loads(config['number_of_files'])

    file_path = "minio://{0}/claimsaudit-ingestfiles/split-input-csv".format(NAMESPACE)
    xr = XpmsResource()
    minio_resource = xr.get(urn=file_path)
    #no of files
    n = number_of_files
    if minio_resource.exists():
        all_files_list = minio_resource.list()
        files_list = [(path.filename) for path in all_files_list if ".csv" in path.fullpath]
        if len(files_list) == 0:

            return {
                "file_path": "na"
            }
        # elif len(files_list) > 1:
        #     return {
        #         'message': 'More than one file present in ' + file_path
        #     }
        else:
            backup_path = "minio://{0}/claimsaudit-ingestfiles/archive/split-input-csv-inprogress".format(
                NAMESPACE)
            path_list = []
            for file_name in files_list[:n]:
                xrm = XpmsResource()
                mr = xrm.get(urn=file_path + '/' + file_name)

                backup_urn = backup_path + '/' + file_name
                backup_rm = XpmsResource()
                backup_mr = backup_rm.get(urn=backup_urn)
                mr.copy(backup_mr)
                mr.delete()
                path_list.append(backup_urn)
            return {

                "file_path": path_list
            }

    else:
        return {

            "file_path": "na"
        }
